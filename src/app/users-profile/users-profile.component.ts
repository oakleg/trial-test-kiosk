import { Component, OnInit, ViewChild, Inject, Input } from '@angular/core';
import {
  MatPaginator, MatSort, TooltipPosition, MatLabel,
  MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig
} from '@angular/material';
import { EXAMPLE_DATA, UsersProfileDataSource, UsersProfileItem } from './users-profile-datasource';
import { FormControl } from '@angular/forms';
import { UserService } from '../user.service';

@Component({
  selector: 'users-profile',
  templateUrl: './users-profile.component.html',
  styleUrls: ['./users-profile.component.css', '../app.component.css']
})
export class UsersProfileComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  userForm: UsersProfileItem;
  action: string;
  displayedColumns = ['id', 'firstName', 'lastName', 'gender', 'action'];
  // dataSource: UsersProfileDataSource
  dataSource: UsersProfileItem[]

  constructor(private userService: UserService, public dialog: MatDialog) { }

  openDialog(action: string, data): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '500px';
    dialogConfig.autoFocus = true;

    dialogConfig.data = action == 'add' ? { ...this.userForm, action: action } : { ...data, action: action };

    let refDialog = this.dialog.open(UsersFormComponent, dialogConfig);
    refDialog.afterClosed().subscribe(res => {
      this.userService.getUsers()
        .subscribe(users => {
          this.dataSource = users
        });
    })
  }

  delete(id) {
    this.userService.deleteUser(id)
    this.dataSource = this.userService.usersData
  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(users => this.dataSource = users);

    //For pagination setup
    // this.dataSource = new UsersProfileDataSource(this.paginator, this.sort);
  }
}

@Component({
  selector: 'user-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-profile.component.css']
})

export class UsersFormComponent implements OnInit {
  constructor(
    private userService: UserService,
    public dialogRef: MatDialogRef<UsersFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UsersProfileItem
  ) { }

  save(data: UsersProfileItem) {
    this.userService.submitUsers(data)
    this.dialogRef.close(data)
  }

  close() {
    this.dialogRef.close()
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}