import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { getTreeNoValidDataSourceError } from '@angular/cdk/tree';
import { UserService } from '../user.service';

export interface UsersProfileItem {
  firstName: string;
  lastName: string;
  gender: string;
  id: number;
}

//Fake data
export const EXAMPLE_DATA: UsersProfileItem[] =
  [{
    id: 1,
    firstName: "Quinn",
    lastName: "Bernaldo",
    gender: "Male"
  }, {
    id: 2,
    firstName: "Maureene",
    lastName: "Benet",
    gender: "Female"
  }, {
    id: 3,
    firstName: "Emmie",
    lastName: "Dodgson",
    gender: "Female"
  }, {
    id: 4,
    firstName: "Mahmoud",
    lastName: "Diss",
    gender: "Male"
  }, {
    id: 5,
    firstName: "Bondie",
    lastName: "Lavender",
    gender: "Male"
  }, {
    id: 6,
    firstName: "Amelina",
    lastName: "Kingescot",
    gender: "Female"
  }, {
    id: 7,
    firstName: "Slade",
    lastName: "Colliard",
    gender: "Male"
  }, {
    id: 8,
    firstName: "Nona",
    lastName: "Cleaveland",
    gender: "Female"
  }, {
    id: 9,
    firstName: "Gherardo",
    lastName: "Stanfield",
    gender: "Male"
  }, {
    id: 10,
    firstName: "Steffie",
    lastName: "Peffer",
    gender: "Female"
  }, {
    id: 11,
    firstName: "Sibel",
    lastName: "Eates",
    gender: "Female"
  }, {
    id: 12,
    firstName: "Antonin",
    lastName: "Juris",
    gender: "Male"
  }, {
    id: 13,
    firstName: "Florenza",
    lastName: "Klisch",
    gender: "Female"
  }, {
    id: 14,
    firstName: "Cyndia",
    lastName: "Wilmot",
    gender: "Female"
  }, {
    id: 15,
    firstName: "Consuela",
    lastName: "Jakaway",
    gender: "Female"
  }
  ];

//Below here is the Setup for pagination for future use 
/**
 * Data source for the UsersProfile view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class UsersProfileDataSource extends DataSource<UsersProfileItem> {


  data: UsersProfileItem[];
  
  constructor(private userService: UserService, private paginator: MatPaginator, private sort: MatSort) {
    super();
  }
  
  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<UsersProfileItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.

    // this.userService.getUsers()
    // .subscribe(users => this.data = users);
    
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginators length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() { }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: UsersProfileItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: UsersProfileItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'firstName': return compare(a.firstName, b.firstName, isAsc);
        case 'lastName': return compare(a.lastName, b.lastName, isAsc);
        case 'gender': return compare(a.gender, b.gender, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
