import { Injectable } from '@angular/core';
import { EXAMPLE_DATA, UsersProfileItem } from './users-profile/users-profile-datasource';
import { Observable, of } from 'rxjs';
import alertjs from 'alertifyjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // TODO TRY TO MESSAGES USING THIS VARIABLE LIKE BELOW
  usersData: UsersProfileItem[] = EXAMPLE_DATA;
  constructor() { }

  submitUsers(data) {
    let users = this.usersData;
    let idNumber = this.usersData.length;
    users = data.action === 'add'
      ? [
        {
          id: ++idNumber,
          firstName: data.firstName,
          lastName: data.lastName,
          gender: data.gender
        },
        ...users
      ]
      : users.map(user =>
        data.id === user.id ? data : user
      );
    this.usersData = users;

    data.action === 'add'
      ? alertjs.success('Successfully Added')
      : alertjs.success('Successfully Updated');
  }

  deleteUser(id) {
    let users = this.usersData;

    users = users.reduce((finalList, user) => {
      if (user.id !== id) {
        finalList.push(user);
      }
      return finalList;
    }, []);

    this.usersData = users;
    alertjs.success('Successfully Deleted');
    return this.usersData;
  }

  getUsers(): Observable<UsersProfileItem[]> {
    return of(this.usersData);
  }
}
