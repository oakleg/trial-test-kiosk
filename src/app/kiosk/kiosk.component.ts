import { Component, OnInit, Inject } from '@angular/core';
import {
  MatPaginator, MatSort, TooltipPosition, MatLabel,
  MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig
} from '@angular/material';
import { FormControl } from '@angular/forms';
import { KioskService } from '../kiosk.service';
import { KioskItem } from './kiosk-datasource';

@Component({
  selector: 'app-kiosk',
  templateUrl: './kiosk.component.html',
  styleUrls: ['./kiosk.component.css', '../app.component.css']
})
export class KioskComponent implements OnInit {
  dataSource: KioskItem[];
  kioskForm: KioskItem;

  displayedColumns = ['id', 'name', 'info', 'action'];

  constructor(private kioskService: KioskService, public dialog: MatDialog) { }

  openDialog(action: string, data): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '500px';
    dialogConfig.autoFocus = true;

    dialogConfig.data = action == 'add' ? { ...this.kioskForm, action: action } : { ...data, action: action };

    let refDialog = this.dialog.open(KioskFormComponent, dialogConfig);
    refDialog.afterClosed().subscribe(res => {
      this.kioskService.getKiosk()
        .subscribe(users => {
          this.dataSource = users
        });
    })
  }

  delete(id) {
    this.kioskService.deleteKiosk(id)
    this.dataSource = this.kioskService.kioskData
  }

  ngOnInit() {
    this.kioskService.getKiosk()
      .subscribe(res => {
        console.log('res: ', res);
        this.dataSource = res
      })
  }

}

@Component({
  selector: 'kiosk-form',
  templateUrl: './kiosk-form.component.html',
  styleUrls: ['./kiosk.component.css', '../app.component.css']
})
export class KioskFormComponent implements OnInit {

  constructor(
    private kioskService: KioskService,
    public dialogRef: MatDialogRef<KioskFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: KioskItem
  ) { }
  
  save(data: KioskItem) {
    this.kioskService.submitKiosk(data)
    this.dialogRef.close(data)
  }

  close() {
    this.dialogRef.close()
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

  }
}