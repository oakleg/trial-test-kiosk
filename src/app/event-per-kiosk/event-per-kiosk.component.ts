import { Component, OnInit, Inject } from '@angular/core';
import {
  MatPaginator, MatSort, TooltipPosition, MatLabel,
  MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig
} from '@angular/material';
import { FormControl } from '@angular/forms';
import { KioskService } from '../kiosk.service';
import { EventPerKioskItem } from './kiosk-per-event-datasource';

@Component({
  selector: 'app-event-per-kiosk',
  templateUrl: './event-per-kiosk.component.html',
  styleUrls: ['./event-per-kiosk.component.css', '../app.component.css']
})
export class EventPerKioskComponent implements OnInit {
  dataSource: EventPerKioskItem[];
  kioskForm: EventPerKioskItem;

  displayedColumns = ['id', 'title', 'description', 'action'];

  constructor(private kioskService: KioskService, public dialog: MatDialog) { }

  openDialog(action: string, data): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '500px';
    dialogConfig.autoFocus = true;

    dialogConfig.data = action === 'add' ? { ...this.kioskForm, action: action } : { ...data, action: action };

    let refDialog = this.dialog.open(EventPerKioskFormComponent, dialogConfig);
    refDialog.afterClosed().subscribe(res => {
      this.kioskService.getEventPerKiosk()
        .subscribe(result => {
          this.dataSource = result
        });
    })
  }

  delete(id) {
    this.kioskService.deleteEventPerKiosk(id);
    this.dataSource = this.kioskService.eventPerKioskData;
  }

  ngOnInit() {
    this.kioskService.getEventPerKiosk()
      .subscribe(res => {
        console.log('res: ', res);
        this.dataSource = res
      })
  }

}

@Component({
  selector: 'event-per-kiosk.component',
  templateUrl: './event-per-kiosk-form.component.html',
  styleUrls: ['./event-per-kiosk.component.css', '../app.component.css']
})
export class EventPerKioskFormComponent implements OnInit {

  constructor(
    private kioskService: KioskService,
    public dialogRef: MatDialogRef<EventPerKioskFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EventPerKioskItem
  ) { }

  save(data: EventPerKioskItem) {
    this.kioskService.submitEventPerKiosk(data)
    this.dialogRef.close(data)
  }

  close() {
    this.dialogRef.close()
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

  }
}