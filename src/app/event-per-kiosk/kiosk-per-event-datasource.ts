export interface EventPerKioskItem {
    title: string;
    description: string;
    id: number;
}

export const EVENT_KIOSK_MOCK_DATA: EventPerKioskItem[] = [{
    id: 1,
    description: "Bacon ipsum dolor amet boudin picanha pork loin jowl frankfurter chicken meatball. Beef alcatra rump buffalo pork salami swine. Tongue picanha cow pig hamburger prosciutto, chuck tail turducken kevin. Pancetta biltong ball tip meatloaf swine bresaola pork belly rump. Pork belly doner pancetta burgdoggen jerky chicken.",
    title: "Failed Production"
}, {
    id: 2,
    description: "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
    title: "Lorem Ip"
}, {
    id: 3,
    description: "Bacon ipsum dolor amet boudin picanha pork loin jowl frankfurter chicken meatball. Beef alcatra rump buffalo pork salami swine. Tongue picanha cow pig hamburger prosciutto, chuck tail turducken kevin. Pancetta biltong ball tip meatloaf swine bresaola pork belly rump. Pork belly doner pancetta burgdoggen jerky chicken.",
    title: "Testing Data"
}, {
    id: 4,
    description: "Est officia minim, deserunt in meatloaf frankfurter meatball voluptate in occaecat sausage ad sunt. Non velit tenderloin turkey, consectetur pork labore ham enim ipsum. Ad salami esse, t-bone ut turkey shankle consectetur pork loin est prosciutto ball tip kielbasa. Commodo porchetta fatback shank cupim, flank buffalo chicken chuck anim turkey sirloin qui spare ribs ex. Tri-tip sunt pork belly shoulder.",
    title: "Mark 10"
}, {
    id: 5,
    description: "Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
    title: "Random Title"
}];