import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { KIOSK_MOCK_DATA, KioskItem } from './kiosk/kiosk-datasource';
import { EVENT_KIOSK_MOCK_DATA, EventPerKioskItem } from './event-per-kiosk/kiosk-per-event-datasource';
import alertjs from 'alertifyjs';

@Injectable({
  providedIn: 'root'
})
export class KioskService {

  constructor() { }
  eventPerKioskData: EventPerKioskItem[] = EVENT_KIOSK_MOCK_DATA;
  kioskData: KioskItem[] = KIOSK_MOCK_DATA;
  action: string;

  submitKiosk(data) {
    let kioskArray = this.kioskData;
    let idNumber = this.kioskData.length;
    kioskArray = data.action === 'add'
      ? [
        {
          id: ++idNumber,
          name: data.name,
          info: data.info
        },
        ...kioskArray
      ]
      : kioskArray.map(kiosk =>
        data.id === kiosk.id ? data : kiosk
      );
    this.kioskData = kioskArray;

    data.action === 'add'
      ? alertjs.success('Successfully Added')
      : alertjs.success('Successfully Updated');
  }

  deleteKiosk(id) {
    let kiosksArray = this.kioskData;

    kiosksArray = kiosksArray.reduce((finalList, kiosk) => {
      if (kiosk.id !== id) {
        finalList.push(kiosk);
      }
      return finalList;
    }, []);

    this.kioskData = kiosksArray;
    alertjs.success('Successfully Deleted');

    return this.kioskData;
  }

  getKiosk(): Observable<KioskItem[]> {
    return of(this.kioskData);
  }

  // Event Per Kiosk services
  submitEventPerKiosk(data) {
    let kioskArray = this.eventPerKioskData;
    let idNumber = this.eventPerKioskData.length;
    kioskArray = data.action === 'add'
      ? [
        {
          id: ++idNumber,
          title: data.title,
          description: data.description
        },
        ...kioskArray
      ]
      : kioskArray.map(kiosk =>
        data.id === kiosk.id ? data : kiosk
      );
    this.eventPerKioskData = kioskArray;
    this.action = data.action;

    data.action === 'add'
      ? alertjs.success('Successfully Added')
      : alertjs.success('Successfully Updated');
  }

  deleteEventPerKiosk(id) {
    let kiosksArray = this.eventPerKioskData;

    kiosksArray = kiosksArray.reduce((finalList, eventPerKiosk) => {
      if (eventPerKiosk.id !== id) {
        finalList.push(eventPerKiosk);
      }
      return finalList;
    }, []);

    this.eventPerKioskData = kiosksArray;
    alertjs.success('Successfully Deleted');
    return this.eventPerKioskData;
  }

  getEventPerKiosk(): Observable<EventPerKioskItem[]> {
    return of(this.eventPerKioskData);
  }
}
