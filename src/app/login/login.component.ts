import { Component, OnInit } from '@angular/core';
import alertifyjs from 'alertifyjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

  login() {
    alertifyjs.success('Successfully Logged In!');
    window.location.href = 'http://localhost:4200/user-profiles';
  }

  ngOnInit() {
  }

}
