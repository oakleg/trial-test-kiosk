import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatFormFieldModule,
  MatOptionModule,
  MatSelectModule,
  MatInputModule,
  MAT_DIALOG_DATA,
} from '@angular/material';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog'
import { UsersProfileComponent, UsersFormComponent } from './users-profile/users-profile.component';
import { KioskComponent, KioskFormComponent } from './kiosk/kiosk.component';
import { EventPerKioskComponent, EventPerKioskFormComponent } from './event-per-kiosk/event-per-kiosk.component';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'user-profiles',
    children: [
      { path: '', component: UsersProfileComponent },
      {
        path: 'form',
        children: [
          { path: '', component: UsersFormComponent },
        ]
      }
    ]
  },
  {
    path: 'kiosk',
    children: [
      { path: '', component: KioskComponent },
      {
        path: 'form',
        children: [
          { path: '', component: KioskFormComponent },
        ]
      }
    ]
  },
  {
    path: 'event-per-kiosk', children: [
      { path: '', component: EventPerKioskComponent },
      {
        path: 'form',
        children: [
          { path: '', component: EventPerKioskFormComponent },
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    AdminPanelComponent,
    UsersProfileComponent,
    KioskFormComponent,
    UsersFormComponent,
    KioskComponent,
    EventPerKioskComponent,
    EventPerKioskFormComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),

    LayoutModule,
    MatInputModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  providers: [
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: {} },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
